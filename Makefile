#
# Makefile for `Hunt the Wumpus' and `Superhack' games
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

prefix?=/usr

CFLAGS = -Wall

all: wumpus superhack

wumpus: wumpus.c
	$(CC) $(CFLAGS) wumpus.c -o wumpus

superhack: superhack.c
	$(CC) $(CFLAGS) superhack.c -o superhack

clean:
	rm -f wumpus wumpus.o superhack superhack.o TAGS *.6 
	rm -f *~ *.1 *.html *.tar.gz MANIFEST

TAGS:
	etags wumpus.c

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .6

.adoc.6:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

SOURCES = README.adoc COPYING NEWS.adoc control Makefile wumpus.c superhack.c wumpus.adoc wumpus.png wumpus.desktop superhack.desktop

install: wumpus superhack wumpus.6
	mkdir -p $(prefix)/bin
	mkdir -p $(prefix)/share/man/man6
	cp wumpus superhack $(prefix)/bin/
	cp wumpus.6 $(prefix)/share/man/man6/

uninstall:
	rm $(prefix)/bin/wumpus $(prefix)/bin/superhack
	rm $(prefix)/share/man/man6/wumpus.6

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i $$(find . -name "*.[ch]")

CPPCHECKOPTS =
cppcheck:
	cppcheck -DREVISION=$(VERS) $(CPPCHECKOPTS) wumpus.c
	cppcheck -DREVISION=$(VERS) $(CPPCHECKOPTS) superhack.c

wumpus-$(VERS).tar.gz: $(SOURCES) wumpus.6
	@ls $(SOURCES) wumpus.6 | sed s:^:wumpus-$(VERS)/: >MANIFEST
	@(cd ..; ln -s wumpus wumpus-$(VERS))
	(cd ..; tar -czf wumpus/wumpus-$(VERS).tar.gz `cat wumpus/MANIFEST`)
	@(cd ..; rm wumpus-$(VERS))

dist: wumpus-$(VERS).tar.gz

release: wumpus-$(VERS).tar.gz wumpus.html wumpus.6
	shipper version=$(VERS) | sh -e -x

refresh: wumpus.html
	shipper -N -w version=$(VERS) | sh -e -x
